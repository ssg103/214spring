#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "sorter.h"


typedef struct node {
    char* value;
    struct node * next;
} node;


void printer(node *);

void push(node *, node *);
