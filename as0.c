#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "sorter.h"
#include "as0.h"

void push(node *head, node *currentNode)
{
    node *ptr = head;
    while(ptr->next != NULL)
    {
        ptr= ptr->next;
    }
    ptr->next = currentNode;
    //sortNodes(head, currentNode);

}

void printer(node *head)
{
    node *ptr = head;
    while(ptr->next != NULL)
    {
        printf("%s\n", ptr->value);
        ptr= ptr->next;
    }
    if(ptr->value != NULL)
        printf("%s\n", ptr->value);
}

int main(int argc, char **argv)
{
    if(argv[1] == NULL)
        return 1;

    int counter = 0;
    char hasFinished = '0';

    char startedCounter = '0';
    int start = 0;
    int end = 0;

    node *head = (node*)malloc(sizeof(node));
    head->value = NULL;

    while(hasFinished == '0')
    {


        if(isalpha(argv[1][counter]))
        {
            if(startedCounter == '0')
            {
                start = counter;
                startedCounter = '1';
            }
        }
        else
        {
            if(startedCounter == '1')
            {
                end = counter;
                startedCounter = '0';

                int loopCount = start;
                if(end>start)
                {
                    char *temp = (char*)malloc(sizeof(char)*(end-start)+1);
                    int tempCount = 0;
                    for(loopCount = start; loopCount< end; loopCount++)
                    {
                        temp[tempCount] = argv[1][loopCount];
                        tempCount++;
                    }
                    temp[end-start+1] = '\0';
                    //printf("%s\n", temp);

                    if(head->value == NULL)
                    {
                        head->value = temp;
                        head->next = NULL;
                    }
                    else
                    {
                        node *_tempNode = (node*)malloc(sizeof(node));
                        _tempNode->value = temp;
                        _tempNode->next = NULL;
                        push(head, _tempNode);
                    }

                }
                //printf("%d %d\n", start, end);
            }
        }
        if(argv[1][counter] == '\0')
            break;
        counter++;

    }

    printf("\nProgram ended\n");
    printer(head);

    return 0;

}
